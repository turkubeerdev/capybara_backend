<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pylon extends Model 
{

    protected $table = 'pylons';
    public $timestamps = true;
    protected $fillable = array('name', 'description', 'depth', 'location_readable', 'lat', 'long');
    public static $rules = [
        "name" => "required",
    ];

    public function hub()
    {
        return $this->belongsTo('Hub');
    }

}