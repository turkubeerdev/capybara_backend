<?php

namespace App\Parsers;

use App\Reading;
use Illuminate\Support\Facades\Log;
use Exception;


class Readings extends Parse {

    /**
    * Readings
     * "id": 36618,
        "createdAt": "2017-08-22T13:57:59.345Z",
        "updatedAt": null,
        "timestamp": "1970-01-18T09:36:50.279Z",
        "payload": "{\"pressure\": \"1010.45 hPa\", \"temperature\": \"26.426 deg C\", \"humidity\": \"42.75 %\"}",
        "device": 1
    */
    public $jsonReadings;

    public $count;

    /**
     * @param string $filePath
     *
     * @return array
     */
    public function parse($filePath)
    {
        try
        {
            $file = file_get_contents($filePath);

            if(!$this->isJson($file)){
                throw new Exception('File not valid JSON');
            }

            /*Need to remove the first 361 data points, badly formatted.*/
            $this->jsonReadings = array_slice(json_decode($file), 361);

            $this->count = count($this->jsonReadings);

            $readings = $this->parseByDevice(1);

            return $readings;


        } catch (\Exception $e)
        {
            echo $e->getMessage() . PHP_EOL;
        }
    }

    private function parseByDevice($id) {
        $readings = [];

        foreach ($this->jsonReadings as $jsonReading)
        {
            if ($jsonReading->device === $id)
            {


                $payload = json_decode($jsonReading->payload, true);

                $reading = new Reading();

                $reading->temperature = (float) substr( $payload['temperature'], 0, -6);
                $reading->humidity =  (float) substr($payload['humidity'], 0, -2);
                $reading->air_pressure =  (float) substr($payload['pressure'],0,-4);
                $reading->timestamp = $jsonReading->createdAt;

                array_push($readings, $reading);
            }
        }

        return $readings;
    }
}

