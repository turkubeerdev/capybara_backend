<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\HybridRelations;
use Illuminate\Foundation\Auth\User as Authenticatable;




/**
 * App\Device
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Reading[] $readings
 * @mixin \Eloquent
 */
class Device extends Authenticatable
{
    use HybridRelations;

    protected $fillable = [
        'name', 'password', 'location',
    ];

    protected $hidden = [
        'password'
    ];

    protected $connection = 'sqlite';

    public function readings()
    {
        return $this->hasMany(Reading::class);
    }

    public function hub()
    {
        return $this->hasOne(Hub::class);
    }
}
