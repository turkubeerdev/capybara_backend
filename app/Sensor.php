<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sensor extends Model 
{

    protected $table = 'sensors';
    public $timestamps = true;
    protected $fillable = array('uuid', 'position');
    public static $rules = [
        "uuid" => "required",
    ];

    public function pylon()
    {
        return $this->belongsTo('Pylon');
    }

    public function hub()
    {
        return $this->belongsTo('Hub');
    }

}