<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Requests\CheckDeviceRequest;
use App\Device;
use App\Hub;
use Config;
use Dingo\Api\Http\Request;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\SignUpRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @resource Auth - Sign-up
 *
 * Resources related to sign-up
 *
 * */
class SignUpController extends Controller
{
    /**
     *  Register a New Device
     *
     *  Must register a new device before a client can be allowed to post readings. You will need to provide a secret token as `secret_client_token`
     *
     * @param SignUpRequest $request
     * @param JWTAuth $JWTAuth
     * @return \Illuminate\Http\JsonResponse
     */
    public function signUp(SignUpRequest $request, JWTAuth $JWTAuth)
    {

        /*For now we will use an environment variable for secret token
        Eventually the token will be related to a Company or Client*/
        if($request->input('secret_client_token') == env('SECRET_CLIENT_TOKEN')) {

            /*TODO add secret token counter*/

            $device = new Device([
                'name' => $request->input('name'),
                'password' => bcrypt($request->input('password')),
            ]);


            if(!$device->save()) {
                throw new HttpException(500);
            }

            //create a new Hub with the name given by the device.
            $device->hub()->create(['name' => $request->input('name')]);

            if(!Config::get('boilerplate.sign_up.release_token')) {

                return response()->json([
                    'status' => 'ok',
                    'device' => $device
                ], 201);
            }

            $token = $JWTAuth->fromUser($device);
            return response()->json([
                'status' => 'ok',
                'token' => $token,
                'device' => $device
            ], 201);
        } else {
            return response()->json([
                'status' => 'Unauthorized'
            ], 401);
        }
    }

    /**
     * Check Device Name
     *
     * Check to ensure that the device name generated is unique
     *
     * @param CheckDeviceRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function check(CheckDeviceRequest $request)
    {
        if(Device::where('name', '=', $request->input('name'))->exists()) {
            return response()->json([
                'status' => 'ok',
                'device' => $request->input('name')
            ],200);
        } else {
            return response()->json([
                'status' => 'ok',
                'device' => false
            ],200);
        }
    }
}
