<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Requests\ReadingRequest;
use App\Device;
use App\Reading;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 *
 * @resource Reading
 *
 * Resources related to storing and viewing senor readings
 */
class ReadingController extends Controller
{

    /**
     * Device Readings Index
     *
     * Fetches all the reading stored for the device by id.
     *
     *
     * @param Request $request
     *
     * @param $device_id
     * @return array
     */
    public function index(Request $request, $device_id)
    {
        $query = (int) $request->query('limit');

        $device = Device::find($device_id);

        if(!$query) {
            return  $device->readings()->get()->toArray();
        }

        return $device->readings()->orderBy('created_at','desc')->take($query)->get()->toArray();
    }


    /**
     * Device Reading Store
     *
     * Store a new reading attached to the device.
     *
     * @param ReadingRequest $request
     * @param $device_id
     * @return \Illuminate\Http\Response
     */
    public function store(ReadingRequest $request, $device_id)
    {
        $reading =  new Reading(json_decode($request->input('reading'), true));

        $device = auth()->user();

        if($device->id != $device_id) {
            return response()->json([
                'status' => 'not_authorized_to_create_reading_for_this_device'
            ], 403);
        }


        if($device->readings()->save($reading))
            return response()->json([
                'status' => 'reading_created',
            ], 200);
        else
            return response()->json([
                'status' => 'could_not_create_reading'
            ], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Reading
     */
    public function show($id)
    {
        $reading = Reading::find($id);

        if(!$reading){
            throw new NotFoundHttpException();
        }

        return $reading;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $reading = Reading::find($id);

        if(!$reading){
            throw new NotFoundHttpException();
        }

        $reading->fill($request->all());

        if($reading->save())
            return response('reading_saved', 200);
         else
            return response('could_not_update_reading', 500);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reading = Reading::find($id);

        if(!$reading){
            throw new NotFoundHttpException();
        }

        if($reading->delete())
            return response('ok', 200);
        else
            return response('could_not_delete_reading', 500);
    }


}
