<?php

namespace  App\Api\V1\Controllers;

use App\Http\Controllers\RESTActions;
use App\Hub;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HubController extends Controller
{
    const MODEL = "App\Hub";

    use RESTActions;
    /**
     * Display a listing of the Hubs with associated pylon information
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = (int) $request->query('limit');

        if(!$query) {
            return Hub::with(['pylons', 'sensors'])->get()->toArray();
        }

        return Hub::orderBy('created_at','desc')->take($query)->get()->toArray();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
