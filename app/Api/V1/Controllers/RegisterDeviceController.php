<?php

namespace App\Api\V1\Controllers;

use App\Device;
use App\Hub;
use Config;
use App\User;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\SignUpRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;


class RegisterDeviceController extends Controller{

    public function register(SignUpRequest $request, JWTAuth $JWTAuth)
    {
        $device = new Device($request->all());
        if ( ! $device->save())
        {
            throw new HttpException(500);
        }

        if ( ! Config::get('boilerplate.sign_up.release_token'))
        {
            return response()->json([
                'status' => 'ok',
            ], 201);
        }


        $token = $JWTAuth->fromUser($device);

        return response()->json([
            'status' => 'ok',
            'token'  => $token,
        ], 201);
    }
}