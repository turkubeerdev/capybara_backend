<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Requests\LoginRequest;
use App\Device;
use Dingo\Api\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 *
 * @resource Auth - Login
 *
 * Login related resources
 */
class LoginController extends Controller
{
    /**
     * Login Device
     *
     * Login a device that returns a JWT token if authenticated
     *
     * @response {
     *  data: [],
     *}
     *
     *
     * @param LoginRequest $request
     * @param JWTAuth $JWTAuth
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request, JWTAuth $JWTAuth)
    {
        $credentials = [
            'name' => $request->input('name'),
            'password' => $request->input('password')
        ];

        try {
            $token = $JWTAuth->attempt($credentials);

            if (!$token) {
                throw new AccessDeniedHttpException();
            }

        } catch (JWTException $e) {
            throw new HttpException(500);
        }

        return response()
            ->json([
                'status' => 'ok',
                'device' => Device::where('name', $request->input('name'))->firstOrFail(),
                'token' => $token
            ]);
    }

    /**
     *  Refresh Token
     *
     * Route to refresh token. Token must be in included in the header as "Authorization: Bearer 'token'"
     *
     *
     * */
    public function refresh()
    {

        return response()->json([
            'message' => 'By accessing this endpoint, you can refresh your access token at each request. Check out this response headers!',

        ]);
    }
}
