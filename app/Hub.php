<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hub extends Model 
{

    protected $table = 'hubs';
    public $timestamps = true;
    protected $fillable = array('name', 'location_readable', 'lat', 'long', 'description', 'setup_complete');
    public static $rules = [

    ];

    public function pylons()
    {
        return $this->hasMany(Pylon::class);
    }

    public function sensors()
    {
        return $this->hasMany(Sensor::class);
    }

    public function device()
    {
        return $this->belongsTo(Device::class);
    }

}