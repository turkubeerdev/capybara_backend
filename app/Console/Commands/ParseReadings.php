<?php

namespace App\Console\Commands;

use App\Device;
use App\Parsers\Readings;
use App\Reading;
use Illuminate\Console\Command;
use League\Flysystem\Exception;

class ParseReadings extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:readings {filePath} {--dropTable}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse JSON files';

    /**
     * The console command description.
     *
     * @var Readings
     */
    protected $readings;

    /**
     * Create a new command instance.
     *
     * @param Readings $readings
     */
    public function __construct(Readings $readings)
    {
        parent::__construct();

        $this->readings = $readings;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        try
        {
            if ( ! $filePath = $this->argument('filePath'))
            {
                $this->info('You need to specify a path for the json file to be parsed.');

                throw new \Exception('No file given');
            }

            $readings = $this->readings->parse($filePath);

            $bar = $this->output->createProgressBar(count($readings));

            $device = Device::find(1);

            foreach ($readings as $reading)
            {
                $device->readings()->save($reading);

                $bar->advance();
            }
        } catch (Exception $e)
        {
            echo $e->getMessage();
        }

    }
}
