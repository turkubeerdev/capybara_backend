<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSensorsTable extends Migration {

	public function up()
	{
		Schema::create('sensors', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('uuid')->unique();
			$table->smallInteger('position')->unsigned()->nullable();
			$table->integer('hub_id')->unsigned();
			$table->integer('pylon_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('sensors');
	}
}