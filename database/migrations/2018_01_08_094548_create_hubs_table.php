<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHubsTable extends Migration {

	public function up()
	{
		Schema::create('hubs', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name');
            $table->text('description')->nullable();
            $table->string('location_readable')->nullable();
			$table->string('lat')->nullable();
			$table->string('long')->nullable();
            $table->boolean('setup_complete')->default(0);
            $table->integer('device_id')->unsigned();
        });
	}

	public function down()
	{
		Schema::drop('hubs');
	}
}