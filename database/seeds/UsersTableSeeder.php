<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User([
            'name'     => 'tester' . str_random(10),
            'email'    => str_random(10) . '@gmail.com',
            'password' => bcrypt('password'),
            'client_id' => 1
        ]);


        $user->save();
    }
}
