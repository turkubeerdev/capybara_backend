<?php

use App\Device;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     *  1. create a new client
     *  2. create a user
     *  3. associate that user with the client
     *  4. add a device to the client
     * @return void
     */
    public function run()
    {
        $client = new \App\Client();

        $client->save();

        $this->call(UsersTableSeeder::class);

        $user = \App\User::find(1);

        $user->client()->associate(1);

        $client->devices()->create([
            'device_name' => 'test_unit_1',
            'password'    => bcrypt('password'),
        ]);
    }
}
