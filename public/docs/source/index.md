---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)
<!-- END_INFO -->

#Auth - Login

Login related resources
<!-- START_7a105803898a6dce2c4c3768ef3f0bbd -->
## Login Device

Login a device that returns a JWT token if authenticated

> Example request:

```bash
curl -X POST "http://capybara.test//api/auth/device/login" \
-H "Accept: application/json" \
    -d "name"="enim" \
    -d "password"="enim" \

```


```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://capybara.test//api/auth/device/login",
    "method": "POST",
    "data": {
        "name": "enim",
        "password": "enim"
},
    "headers": {
        "accept": "application/json"
        "Authorization": "Bearer {token}"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response

```json
{
    "status": "ok",
    "device": {
        "id": 10,
        "name": "test2",
        "client_id": null,
        "location": null,
        "ip_address": null,
        "system_info": null,
        "created_at": "2018-02-04 19:08:01",
        "updated_at": "2018-02-04 19:08:01"
    },
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpc3MiOiJodHRwOi8vY2FweWJhcmEudGVzdC9hcGkvYXV0aC9kZXZpY2UvbG9naW4iLCJpYXQiOjE1MTc4MTc1MjAsImV4cCI6MTUxNzgyMTEyMCwibmJmIjoxNTE3ODE3NTIwLCJqdGkiOiI0QW5EY1g4bHFqTzVRZXR6In0.Hf15DRqwlQwketvcr2Tpz0OPduE6oZUE_FBX7gb4jmA"
}
```


### HTTP Request
`POST /api/auth/device/login`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | 
    password | string |  required  | 

<!-- END_7a105803898a6dce2c4c3768ef3f0bbd -->

<!-- START_56975e0ce013a7a2e07acd9e0977a7e7 -->
## Refresh Token

Route to refresh token. Token must be in included in the header as "Authorization: Bearer 'token'"

> Example request:

```bash
curl -X GET "http://capybara.test//api/refresh" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://capybara.test//api/refresh",
    "method": "GET",
    "headers": {
        "accept": "application/json"
        "Authorization": "Bearer {token}"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "message": "By accessing this endpoint, you can refresh your access token at each request. Check out this response headers!"
}
```

### HTTP Request
`GET /api/refresh`

`HEAD /api/refresh`


<!-- END_56975e0ce013a7a2e07acd9e0977a7e7 -->
#Auth - Sign-up

Resources related to sign-up
<!-- START_2f509da665de6e99186ed354d843e054 -->
## Register a New Device

Must register a new device before a client can be allowed to post readings. You will need to provide a secret token as `secret_client_token`

> Example request:

```bash
curl -X POST "http://capybara.test//api/auth/device/signup" \
-H "Accept: application/json" \
    -d "name"="et" \
    -d "password"="et" \
    -d "secret_client_token"="et" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://capybara.test//api/auth/device/signup",
    "method": "POST",
    "data": {
        "name": "et",
        "password": "et",
        "secret_client_token": "et"
},
    "headers": {
        "accept": "application/json"
        "Authorization": "Bearer {token}"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response

```json
{
    "status": "ok",
    "device": {
        "name": "test2",
        "updated_at": "2018-02-04 19:08:01",
        "created_at": "2018-02-04 19:08:01",
        "id": 10
    }
}
```


### HTTP Request
`POST /api/auth/device/signup`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | 
    password | string |  required  | 
    secret_client_token | string |  required  | 

<!-- END_2f509da665de6e99186ed354d843e054 -->

<!-- START_e964b8ed1791a6fc80db719ac3223a84 -->
## Check Device Name

Check to ensure that the device name generated is unique

> Example request:

```bash
curl -X POST "http://capybara.test//api/auth/device/check" \
-H "Accept: application/json" \
    -d "name"="distinctio" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://capybara.test//api/auth/device/check",
    "method": "POST",
    "data": {
        "name": "distinctio"
},
    "headers": {
        "accept": "application/json"
        "Authorization": "Bearer {token}"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

 > Example response:
 
 ```json
 {
     "status": "ok",
     "device": false
 }
```


### HTTP Request
`POST /api/auth/device/check`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | 

<!-- END_e964b8ed1791a6fc80db719ac3223a84 -->

#Reading

Resources related to storing and viewing senor readings
<!-- START_5165fd6ca0c9796762de49ac14c141b2 -->
## Device Reading Store

Store a new reading attached to the device.

> Example request:

```bash
curl -X POST "http://capybara.test//api/device/{device_id}/readings" \
-H "Accept: application/json" \
    -d "reading"="[&quot;foo&quot;,&quot;bar&quot;,&quot;baz&quot;]" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://capybara.test//api/device/{device_id}/readings",
    "method": "POST",
    "data": {
        "reading": "[\"foo\",\"bar\",\"baz\"]"
},
    "headers": {
        "accept": "application/json"
        "Authorization": "Bearer {token}"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

 > Example response:
 
 ```json
 {
     "status": "reading_created"
 }
 ```


### HTTP Request
`POST /api/device/{device_id}/readings`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    reading | string |  required  | Must be a valid JSON string.

<!-- END_5165fd6ca0c9796762de49ac14c141b2 -->

<!-- START_feecb874220ae36a17d8748f7fa264b3 -->
## Device Readings Index

Fetches all the reading stored for the device by id.

> Example request:

```bash
curl -X GET "http://capybara.test//api/device/{device_id}/readings" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://capybara.test//api/device/{device_id}/readings",
    "method": "GET",
    "headers": {
        "accept": "application/json"
        "Authorization": "Bearer {token}"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "exception": null,
    "headers": {},
    "original": [
        {
            "_id": "5a7752e29a8920065375c3a4",
            "reading": {
                "sensors": [
                    {
                        "uuid": "00000829d13b_0",
                        "value": 21.5
                    },
                    {
                        "uuid": "0000082a1a86_0",
                        "value": 20.5
                    },
                    {
                        "uuid": "000008297f5f_0",
                        "value": 23
                    },
                    {
                        "uuid": "00000829e7dc_0",
                        "value": 20.5
                    },
                    {
                        "uuid": "00000829d614_0",
                        "value": 21.5
                    },
                    {
                        "uuid": "00000829a5e2_0",
                        "value": 21
                    },
                    {
                        "uuid": "0000085aff47_0",
                        "value": 20.5
                    }
                ]
            },
            "device_id": 9,
            "updated_at": "2018-02-04 18:37:22",
            "created_at": "2018-02-04 18:37:22"
        },
        {
            "_id": "5a7752e59a89200a81784194",
            "reading": {
                "sensors": [
                    {
                        "uuid": "00000829d13b_0",
                        "value": 21.5
                    },
                    {
                        "uuid": "0000082a1a86_0",
                        "value": 20.5
                    },
                    {
                        "uuid": "000008297f5f_0",
                        "value": 23
                    },
                    {
                        "uuid": "00000829e7dc_0",
                        "value": 20.5
                    },
                    {
                        "uuid": "00000829d614_0",
                        "value": 21.5
                    },
                    {
                        "uuid": "00000829a5e2_0",
                        "value": 21
                    },
                    {
                        "uuid": "0000085aff47_0",
                        "value": 20.5
                    }
                ]
            },
            "device_id": 9,
            "updated_at": "2018-02-04 18:37:25",
            "created_at": "2018-02-04 18:37:25"
        },
        {
            "_id": "5a7753839a89200654407574",
            "reading": {
                "sensors": [
                    {
                        "uuid": "00000829d13b_0",
                        "value": 21.5
                    },
                    {
                        "uuid": "0000082a1a86_0",
                        "value": 20.5
                    },
                    {
                        "uuid": "000008297f5f_0",
                        "value": 23
                    },
                    {
                        "uuid": "00000829e7dc_0",
                        "value": 20.5
                    },
                    {
                        "uuid": "00000829d614_0",
                        "value": 21.5
                    },
                    {
                        "uuid": "00000829a5e2_0",
                        "value": 21
                    },
                    {
                        "uuid": "0000085aff47_0",
                        "value": 20.5
                    }
                ]
            },
            "device_id": 9,
            "updated_at": "2018-02-04 18:40:03",
            "created_at": "2018-02-04 18:40:03"
        },
        {
            "_id": "5a7758ba9a8920065375c3a5",
            "reading": {
                "sensors": [
                    {
                        "uuid": "00000829d13b_0",
                        "value": 22.5
                    },
                    {
                        "uuid": "0000082a1a86_0",
                        "value": 21.5
                    },
                    {
                        "uuid": "000008297f5f_0",
                        "value": 24
                    },
                    {
                        "uuid": "00000829e7dc_0",
                        "value": 21.5
                    },
                    {
                        "uuid": "00000829d614_0",
                        "value": 22.5
                    },
                    {
                        "uuid": "00000829a5e2_0",
                        "value": 23
                    },
                    {
                        "uuid": "0000085aff47_0",
                        "value": 21.5
                    }
                ]
            },
            "device_id": 9,
            "updated_at": "2018-02-04 19:02:18",
            "created_at": "2018-02-04 19:02:18"
        },
        {
            "_id": "5a7759de9a89200a81784195",
            "sensors": [
                {
                    "uuid": "00000829d13b_0",
                    "value": 22.5
                },
                {
                    "uuid": "0000082a1a86_0",
                    "value": 21.5
                },
                {
                    "uuid": "000008297f5f_0",
                    "value": 24
                },
                {
                    "uuid": "00000829e7dc_0",
                    "value": 21.5
                },
                {
                    "uuid": "00000829d614_0",
                    "value": 22.5
                },
                {
                    "uuid": "00000829a5e2_0",
                    "value": 23
                },
                {
                    "uuid": "0000085aff47_0",
                    "value": 21.5
                }
            ],
            "device_id": 9,
            "updated_at": "2018-02-04 19:07:10",
            "created_at": "2018-02-04 19:07:10"
        }
    ]
}
```

### HTTP Request
`GET /api/device/{device_id}/readings`

`HEAD /api/device/{device_id}/readings`


<!-- END_feecb874220ae36a17d8748f7fa264b3 -->

#general
<!-- START_6af0f2ec58678394980e08a917e0bdfb -->
## /api/auth/device/recovery

> Example request:

```bash
curl -X POST "http://capybara.test//api/auth/device/recovery" \
-H "Accept: application/json" \
    -d "email"="wdibbert@example.net" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://capybara.test//api/auth/device/recovery",
    "method": "POST",
    "data": {
        "email": "wdibbert@example.net"
},
    "headers": {
        "accept": "application/json"
        "Authorization": "Bearer {token}"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST /api/auth/device/recovery`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    email | email |  required  | 

<!-- END_6af0f2ec58678394980e08a917e0bdfb -->

<!-- START_5a35996aa12894ff595417754decdf42 -->
## /api/auth/device/reset

> Example request:

```bash
curl -X POST "http://capybara.test//api/auth/device/reset" \
-H "Accept: application/json" \
    -d "token"="quis" \
    -d "email"="jeremie.blanda@example.net" \
    -d "password"="quis" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://capybara.test//api/auth/device/reset",
    "method": "POST",
    "data": {
        "token": "quis",
        "email": "jeremie.blanda@example.net",
        "password": "quis"
},
    "headers": {
        "accept": "application/json"
        "Authorization": "Bearer {token}"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST /api/auth/device/reset`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    token | string |  required  | 
    email | email |  required  | 
    password | string |  required  | 

<!-- END_5a35996aa12894ff595417754decdf42 -->

