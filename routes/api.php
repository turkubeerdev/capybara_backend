<?php

use App\Api\V1\Controllers\ReadingController;
use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['prefix' => 'auth/device'], function(Router $api) {
        $api->post('signup', 'App\\Api\\V1\\Controllers\\SignUpController@signUp');
        $api->post('check', 'App\\Api\\V1\\Controllers\\SignUpController@check');

        $api->post('login', 'App\\Api\\V1\\Controllers\\LoginController@login');

        $api->post('recovery', 'App\\Api\\V1\\Controllers\\ForgotPasswordController@sendResetEmail');
        $api->post('reset', 'App\\Api\\V1\\Controllers\\ResetPasswordController@resetPassword');
        
    });

    $api->group(['middleware' => 'jwt.auth'], function(Router $api) {
        $api->post('device/{device_id}/readings', 'App\\Api\\V1\\Controllers\\ReadingController@store');

        $api->get('refresh', 'App\\Api\\V1\\Controllers\\LoginController@refresh')->middleware(['jwt.refresh']);
    });



    $api->get('device/{device_id}/readings', 'App\\Api\\V1\\Controllers\\ReadingController@index');

});
